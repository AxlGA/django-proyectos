from django.db import models

# Create your models here.
class user(models.Model):
    nombre_usuario=models.CharField(max_length=50)
    apellido=models.CharField(max_length=40)
    email = models.EmailField(unique="true")
    username=models.CharField(max_length=12)
    descripcion=models.TextField()
    isAdmin = models.BooleanField()
    fechar_creacion = models.DateTimeField()

class Nacimientos(models.Model):
    nombres=models.CharField( max_length=50)
    apellidos=models.CharField( max_length=50)
    cui=models.IntegerField()
    fecha_nacimiento=models.DateField()
    lugar_nacimiento=models.CharField(max_length=50)

    nombres_mama=models.CharField( max_length=50)
    apellidos_mama=models.CharField( max_length=50)
    cui_mama=models.IntegerField()
    fecha_nacimiento_mama=models.DateField()
    lugar_nacimiento_mama=models.CharField(max_length=50)

    nombres_papa=models.CharField( max_length=50)
    apellidos_papa=models.CharField( max_length=50)
    cui_papa=models.IntegerField()
    fecha_nacimiento_papa=models.DateField()
    lugar_nacimiento_papa=models.CharField(max_length=50)

    fechar_modificacion = models.DateTimeField()
