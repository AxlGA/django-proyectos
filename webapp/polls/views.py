from django.shortcuts import render
from django.http import HttpResponse
import json
import time
import datetime



posts = [
    {

        'author': 'CoreyMs',
        'title': 'Blog Post 1',
       'content': 'First post content',
        'date_posted': '27 de Marzo , 2020'
    },
    {

        'author': 'Jane Doe',
        'title': 'Blog Post 2',
        'content': 'Second post content',
        'date_posted': '27 de Marzo , 2020'
    }
]

# Create your views here.
def index(request):
    return HttpResponse("Hola Mundo10SsT")

def numeros(request):

    #ts=time.time()
    ts=time.gmtime()
    #hora=datetime.datetime.fromtimestamp(ts).isoformat()
    hora=time.strftime("%x %X", ts)
    cadena='si entro'
    lista=request.GET['array']
    lista= lista.split(',')
    lista=[int(cont) for cont in lista]

    data={
        'status' : 'ok',
        'responde' :'array de numeros',
        'clase': 'Proyectos 980',
        'lista': sorted(lista),
        'mensaje': 'Terminado proceso',
        'Hora': hora,


    }

    return HttpResponse(json.dumps(data), content_type=("application/json"))
    #return HttpResponse(cadena)

def home(request):
    context={
        'posts': posts
    }
    return render(request, 'polls/home.html', context)


def about(request):
    return render(request, 'polls/about.html', {'title':'about'})

def carne(request):
    listName=request.GET['Nombre']
    #ts=time.gmtime()
    #tiempo=time.time()
    #hora=time.localtime(tiempo)
    ts=time.localtime()
    hora=time.strftime("%x %X", ts)
   # hora=time.strftime("%d/%m/%y", time.localtime())

    data=[{
        'alias':'USAC',
        'universidad' : 'Universidad de San Carlos de Guatemala',
        'status' :'Estudiante',
        'carne': '201611587',
        'nombre': listName,
        'dpi': '2995887180101',
        'hora': hora,

    }]

    context={
        'data':data
    }
    return render(request, 'polls/proyectos.html',context)


def carne2(request):

    obtenerNombre=request.GET['Nombre']
    #ts=time.localtime()
    #hora=time.strftime("%x %X", ts)
    hora=time.strftime("%x %X",time.localtime()) 

    data=[{
        'alias':'USAC',
        'universidad' : 'Universidad de San Carlos de Guatemala',
        'status' :'Estudiante',
        'carne': '201611587',
        'nombre': obtenerNombre,
        'dpi': '2995887180101',
        'hora': hora,
  

    }]

    context={
        'data':data
    }



    return render(request, 'polls/hola.html',context)
