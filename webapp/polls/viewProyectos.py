from django.shortcuts import render
from django.http import HttpResponse
import datetime
import time
import json
#articulos = []
articulos = [
    {
        'nombre' :  'Generador de onda',
        'codigo' :  '001',
        'imagen' : 'https://www.instrumentacionhoy.com/imagenes/2013/07/TFG-3605E.jpg',
        'fecha' : time.strftime("%x %X", time.localtime())
    },
    {
        'nombre' :  'Osciloscopio',
        'codigo' :  '001',
        'imagen' : 'https://www.instrumentacionhoy.com/imagenes/2013/07/TFG-3605E.jpg',
        'fecha' : time.strftime("%x %X", time.localtime())
    },

]

def lista_articulos(request):
    content = []
    for articulo in articulos:
        content.append("""
            
            <p><strong>{nombre}</strong></p>
            <p><small>{nombre}</small></p>
            <figure><img src="{imagen}"/></figure>   
    
        """.format(**articulo))
    #return HttpResponse('<br>'.join(content))
    return render(request, 'polls/articulos.html',{"lista": articulos})