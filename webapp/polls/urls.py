from django.urls import path

from . import views
from . import viewProyectos

urlpatterns = [
    #path('', views.index, name='index'),
    path('', views.numeros, name='numeros'),
    path('first/', views.index, name='index'),
    path('home/', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('carne/', views.carne, name='carne'),

    path('hola/', views.carne2, name='carne2'),

    path('articulos/', viewProyectos.lista_articulos, name='articulos'),
]
