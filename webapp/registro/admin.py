from registro.models import UsuarioRegistrado
from django.contrib import admin

# Register your models here.
@admin.register(UsuarioRegistrado)

class UsuarioRegistradoAdmin(admin.ModelAdmin):
    list_display = ('user', 'phone','about')
