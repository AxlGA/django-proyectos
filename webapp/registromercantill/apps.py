from django.apps import AppConfig


class RegistromercantillConfig(AppConfig):
    name = 'registromercantill'
    verbose_name = 'RegistroMercantill'