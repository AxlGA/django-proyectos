from registromercantill.models import UsuarioMercantilRegistrado
from django.contrib import admin

# Register your models here.
@admin.register(UsuarioMercantilRegistrado)

class UsuarioMercantilRegistradoAdmin(admin.ModelAdmin):
    list_display = ('user','fecha_nacimiento','Nacionalidad','Profesion','direccion_domicilio', 'phone','dpi','email','foto','about')