from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class UsuarioMercantilRegistrado(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    fecha_nacimiento=models.DateField()
    Nacionalidad=models.CharField( max_length=50)
    Profesion=models.CharField( max_length=50)
    direccion_domicilio=models.CharField( max_length=50)
    phone = models.CharField(max_length=9,blank=True)
    dpi=models.IntegerField()
    email = models.EmailField(unique="true")
    foto = models.ImageField(upload_to='registro/imagenes',blank=True,null=True)
    about = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username